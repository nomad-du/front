const path = require('path');
const dotenv = require('dotenv');
const webpack = require('webpack');

dotenv.config();

const SOCKET_URL = (() => {
  if (!process.env.SOCKET_URL) {
    throw new Error('Need SOCKET_URL');
  }

  console.log('SOCKET_URL=', process.env.SOCKET_URL);
  return process.env.SOCKET_URL;
})();

const API_URL = (() => {
  if (!process.env.API_URL) {
    throw new Error('Need API_URL');
  }

  console.log('API_URL=', process.env.API_URL);
  return process.env.API_URL;
})();

module.exports = {
  entry: './src/index.jsx',
  output: {
    path:       path.resolve(__dirname, 'build'),
    publicPath: 'build',
    filename:   'app.bundle.js',
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                'es2017',
                'es2015',
                'stage-2',
                'react'
              ],
              plugins: [
                'transform-es2015-parameters',
              ]
            },
        }
        ],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ],
      }
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      SOCKET_URL: JSON.stringify(SOCKET_URL),
      API_URL:    JSON.stringify(API_URL),
    })
  ],
  resolve: {
    modules: [
      path.resolve(__dirname, './src'),
      path.resolve(__dirname, './node_modules'),
    ],
    extensions: ['.js', '.jsx', '.json', '.css'],
  },
  devtool: 'source-map',
  devServer: {
    compress:         true,
    port:             9000,
    contentBase:      path.join(__dirname),
    compress:         true,
    watchContentBase: true,
  },
  node: {
    console: true,
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
};
