import {
  getMoodColorArduino,
} from './lib/moods';

import {
  sendActive,
  sendGeolocation,
  sendDoNotDisturb,
  sendMood,
  getUserState,
} from './lib/api';

import { createBluetoothListener } from './lib/bluetooth';

const NOTIF_DURATION = 8000;

let bluetoothListener = null;

const sendMoodToBT = (mood) => {
  if (!bluetoothListener) {
    return;
  }

  const moodColor = getMoodColorArduino(mood);

  if (bluetoothListener) {
    const msg = `rgb.${moodColor.r}.${moodColor.g}.${moodColor.b}`;
    console.log('send mood to bluetooth', msg);
    bluetoothListener.sendCharacteristic(msg);
  }
};

export const SET_USER = 'SET_USER';
export const setUser = (userId) => ({
  type: SET_USER,
  id:   userId,
});

export const ADD_NEW_USER = 'ADD_NEW_USER';
export const addNewUser = (userId, user) => ({
  type: ADD_NEW_USER,
  id:   userId,
  user,
});

export const SAVE_UPDATE_USER = 'SAVE_UPDATE_USER';
const saveUpdateUser = (userId, update) => ({
  type: SAVE_UPDATE_USER,
  id:   userId,
  user: update,
});

export const NEW_NOTIF = 'NEW_NOTIF';
const newNotif = (id, user) => ({
  type: NEW_NOTIF,
  id,
  user,
});

export const REMOVE_NOTIF = 'REMOVE_NOTIF';
const removeNotif = (id) => ({
  type: REMOVE_NOTIF,
  id,
});

export const UPDATE_USER = 'UPDATE_USER';
export const updateUser = (userId, update) => (dispatch, getState) => {
  dispatch(saveUpdateUser(userId, update));

  const { users, me } = getState();
  const user = users[userId];

  if (!user) {
    sendMoodToBT(me.mood);
  }

  if (!user) return; // update is about current user, no notif

  const notifId = userId;

  dispatch(newNotif(notifId, user));

  const moodColor = getMoodColorArduino(user.mood);
  if (bluetoothListener) {
    const msg = `notif.${moodColor.r}.${moodColor.g}.${moodColor.b}`;
    console.log('send notif to bluetooth', msg);
    bluetoothListener.sendCharacteristic(msg);
  }

  setTimeout(() => dispatch(removeNotif(notifId)), NOTIF_DURATION);
};

export const login = (userId) => (dispatch) => {
  sendActive(userId, true)
    .then((publicState) => dispatch(saveUpdateUser(userId, publicState)));
};

export const logout = (userId) => (dispatch) => {
  sendActive(userId, false)
    .then((publicState) => dispatch(saveUpdateUser(userId, publicState)));
};

export const doNotDisturbOn = (userId) => (dispatch) => {
  sendDoNotDisturb(userId, true)
    .then((publicState) => dispatch(saveUpdateUser(userId, publicState)));
};

export const doNotDisturbOff = (userId) => (dispatch) => {
  sendDoNotDisturb(userId, false)
    .then((publicState) => dispatch(saveUpdateUser(userId, publicState)));
};

export const doNotDisturbToggle = () => (dispatch, getState) => {
  const { me } = getState();

  if (me.doNotDisturb) {
    dispatch(doNotDisturbOff(me.id));
  } else {
    dispatch(doNotDisturbOn(me.id));
  }
};

export const updateGeolocation = (id, coords) => () => {
  sendGeolocation(id, coords);
};

export const loadUserState = (userId) => (dispatch) => {
  getUserState(userId)
    .then((state) => dispatch(saveUpdateUser(userId, state)));
};

export const listenMoodChange = () => (dispatch, getState) => {
  const { me : { id, mood } } = getState();

  if (!bluetoothListener) {
    createBluetoothListener()
      .then((listener) => {
        bluetoothListener = listener;

        bluetoothListener.onCharacteristicChange((value) => {
          if (value.startsWith('pot.')) {
            const potValue = parseInt(value.replace('pot.', ''), 10);
            const moodValue = Math.floor(potValue / 50);

            console.log('send new moodValue', moodValue);
            sendMood(id, moodValue);
          } else if (value.startsWith('button.')) {
            dispatch(doNotDisturbToggle());
          }
        });

        sendMoodToBT(mood);
      });
  }
};

export const SHOW_MODAL = 'SHOW_MODAL';
export const showModal = (modalState) => ({
  type:  SHOW_MODAL,
  modal: modalState,
});

export const updateMood = (userId, mood) => () => {
  sendMood(userId, mood);
};
