import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import queryString from 'query-string';

import configureStore from './store';
import App from './App';

import {
  setUser,
  loadUserState,
  listenMoodChange,
  showModal,
} from './actions';

import websocket from './lib/socket';

import '../styles/app.scss';

const USERS = ['1', '2', '3', '4', '5']; // team

const { user, bluetooth } = queryString.parse(window.location.search);
console.log('user param =', user);
console.log('bluetooth param =', bluetooth);

const store = configureStore();
store.dispatch(setUser(user || '1'));
USERS.forEach((userId) => store.dispatch(loadUserState(userId)));

store.dispatch(showModal(!!bluetooth));

const handleBluetoothOk = () => {
  console.log('switch modal off');
  store.dispatch(showModal(false));
  store.dispatch(listenMoodChange());
};

websocket(store);

ReactDom.render(
  <Provider store={store}>
    <App onModalClose={handleBluetoothOk} />
  </Provider>,
  document.querySelector('.js-app')
);
