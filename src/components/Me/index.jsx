/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { Fragment } from 'react';
import cs from 'classnames';
import keys from 'lodash/keys';
import map from 'lodash/map';

import Location from '../Location';
import Image from '../Image';

import './Me.scss';

const MOODS = {
  Normal:           'white',
  'Hello!':         'blue',
  Coooool:          'green',
  Grrr:             'red',
  'Besoin d\'aide': 'yellow',
};

const MoodBar = ({ onAction }) => (
  <div className="me-actions">
    <div className="me-action mod-mood">
      { map(keys(MOODS), (key, i) => (
        <button className="mood-button" key={key} onClick={() => onAction(i)}>
          <span>{key}</span>
          <br />
          <span className={cs('mood', `mod-${MOODS[key]}`)} />
        </button>
      ))}
    </div>
  </div>
);

const ActionBar1 = ({ onAction }) => (
  <div className="me-actions">
    <div className="me-action mod-button">
      <button className="action-button" onClick={() => onAction(3)}>
        Modifier mon <br /> humeur
      </button>
    </div>
    <div className="me-action mod-button">
      <button className="action-button" onClick={() => onAction(2)}>
        Modifier ma <br /> disponibilité
      </button>
    </div>
  </div>
);

const ActionBar2 = ({ doNotDisturb, onAction }) => (
  <div className="me-actions">
    <div className="me-action mod-button mod-rotate">
      <button className="action-button mod-round" onClick={() => onAction(0)}>
        <i className="material-icons">brightness_2</i>
      </button>
    </div>
    <div className="me-action mod-button">
      <button
        className="action-button mod-round"
        onClick={() => onAction(doNotDisturb ? 2 : 1)}
      >
        { doNotDisturb && <i className="material-icons">check</i> }
        { !doNotDisturb && <i className="material-icons">close</i> }
      </button>
    </div>
  </div>
);

class Me extends React.Component {
  constructor() {
    super();

    this.state = {
      bar: 0,
    };

    this.toggleBarHandler = this.toggleBarHandler.bind(this);
    this.actionBar1Handler = this.actionBar1Handler.bind(this);
    this.actionBar2Handler = this.actionBar2Handler.bind(this);
    this.actionBar3Handler = this.actionBar3Handler.bind(this);
  }

  toggleBarHandler() {
    this.setState({ bar: this.state.bar ? 0 : 1 });
  }

  actionBar1Handler(action) {
    this.setState({ bar: action });
  }

  actionBar2Handler(action) {
    if (action === 0) {
      this.props.logoutClick();
    } else if (action === 1) {
      this.props.doNotDisturbClick(true);
    } else {
      this.props.doNotDisturbClick(false);
    }

    this.setState({ bar: 0 });
  }

  actionBar3Handler(mood) {
    this.props.updateMood(mood);
    this.setState({ bar: 0 });
  }

  timeToStr() {
    const { now } = this.props;
    const [hour, min] = [
      `0${now.getHours()}`.slice(-2),
      `0${now.getMinutes()}`.slice(-2),
    ];

    return `${hour}:${min}`;
  }

  render() {
    const { user } = this.props;

    return (
      <div className="me">
        <div className="me-img-wrapper" onClick={this.toggleBarHandler}>
          <span className="clock">{this.timeToStr()}</span>
          <div className="me-image">
            <Image img={user.img} mood={user.mood} big />
            <Location className="me-location" location={user.location} />
          </div>
        </div>
        { this.state.bar === 0 && (
          <div className="me-actions" onClick={this.toggleBarHandler} />
        )}
        { this.state.bar === 1 && (
          <ActionBar1 onAction={this.actionBar1Handler} />
        )}
        { this.state.bar === 2 && (
          <ActionBar2
            onAction={this.actionBar2Handler}
            doNotDisturb={user.doNotDisturb}
          />
        )}
        { this.state.bar === 3 && (
          <MoodBar onAction={this.actionBar3Handler} />
        )}
      </div>
    );
  }
}

class Timer extends React.Component {
  constructor() {
    super();
    this.state = { now: new Date() };

    setInterval(() => {
      this.tick();
    }, 1000); // every seconds
  }

  tick() {
    this.setState({ now: new Date() });
  }

  render() {
    return (
      <Fragment>
        {this.props.render({ now: this.state.now })}
      </Fragment>
    );
  }
}

const MeTimer = (props) => (
  <Timer
    render={({ now }) => (
      <Me now={now} {...props} />
    )}
  />
);

export default MeTimer;

