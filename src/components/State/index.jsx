import React from 'react';
import cs from 'classnames';

import Activity from '../Activity';
import Location from '../Location';
import Image from '../Image';

import './State.scss';

const State = ({ user, border = true }) => (
  <div
    className={cs(
      'state',
      {
        'mod-off':          !user.active,
        'mod-border':       border,
        'mod-doNotDisturb': (
          user.activity && user.activity.type === 'doNotDisturb'
        ),
      }
    )}
  >
    <div className="state-user">
      <Image img={user.img} mood={user.mood} />
      <div className="state-user-info">
        <div className="state-user-info-name">
          { user.name }
        </div>
        <Location location={user.active && user.location} />
      </div>
    </div>
    <div className="state-activity">
      <Activity
        active={user.active}
        doNotDisturb={user.doNotDisturb}
        activity={user.activity}
      />
    </div>
  </div>
);

export default State;
