import React from 'react';

import './InfoBar.scss';

const InfoBar = () => (
  <div className="info-bar">
    <i className="material-icons info-icon">info_outline</i>
    <p className="info-text">2 de vos collègues sont à Clisson</p>
  </div>
);

export default InfoBar;
