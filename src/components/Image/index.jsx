import React from 'react';
import cs from 'classnames';

import './Image.scss';
import { getMoodColorStr } from '../../lib/moods';

const Image = ({ img, mood = false, big = false }) => (
  <div className="image">
    <div
      className={cs(
        'image-wrapper',
        { 'mod-border': mood, 'mod-big': big }
      )}
      style={{
        borderColor:     mood && getMoodColorStr(mood),
        backgroundImage: `url(${img})`,
      }}
    />
  </div>
);

export default Image;
