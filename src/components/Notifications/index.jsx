import React from 'react';

import State from '../State';

import './Notifications.scss';

const NotificationsWrapper = ({ users }) => (
  <div className="notifications">
    <div className="notifications-wrapper">
      { users.map((user) => (
        <State key={user.user_id} user={user} border={false} />
      )) }
    </div>
  </div>
);

const Notifications = ({ users = [], ...props }) => {
  if (!users.length) {
    return null;
  }

  return <NotificationsWrapper users={users} {...props} />;
};

export default Notifications;
