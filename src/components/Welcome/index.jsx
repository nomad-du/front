import React from 'react';

import './Welcome.scss';

const GREETINGS = [
  'Bonjour',
  'Hola!',
  'Salut',
  'Content de vous revoir',
  'Ça faisait longtemps',
  '👋',
];

const ACTIVITY = [
  'RAS. Rien de nouveau depuis votre dernière connexion.',
  'Vos collègues sont très actifs. Il doit se passer quelque chose.',
  `Voici ce que vous avez manqué depuis votre dernière connexion : Julien
  était à Rennes pour 3 jours, et Julie était visiblement en congés. Pas
  d’activité inabituelle si ce n’est beaucoup de réunions.`,
  'RAS. 🤖',
];

const getRandom = (what) => {
  const l = what.length;
  const i = Math.floor(Math.random() * (l - 1));

  return what[i];
};

const Welcome = ({ user, connectUser }) => (
  <div className="welcome">
    <div className="welcome-wrapper">
      <p className="welcome-title">{ getRandom(GREETINGS) } { user.name },</p>
      <p className="welcome-content">{ getRandom(ACTIVITY) }</p>
      <div className="welcome-content mod-center">
        <button className="connect-btn" onClick={connectUser}>
          Se connecter
        </button>
      </div>
    </div>
  </div>
);

export default Welcome;
