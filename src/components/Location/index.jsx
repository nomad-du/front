import React from 'react';

import cs from 'classnames';

import './Location.scss';

const LOCATION_TO_ICON = {
  company:  'business',
  telework: 'home',
  other:    'public',
};

const getLocationIcon = (location) => (
  LOCATION_TO_ICON[location] || LOCATION_TO_ICON.other
);

const Location = ({ location, className }) => {
  if (!location) {
    return null;
  }

  return (
    <div className={cs('location', className)}>
      <i className="material-icons location-elem location-icon">
        { getLocationIcon(location.type) }
      </i>
      {
        location.name &&
        <span className="location-elem location-info">{ location.name }</span>
      }
    </div>
  );
};

export default Location;
