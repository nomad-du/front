import React, { Fragment } from 'react';
import moment from 'moment';
import cs from 'classnames';

import './Activity.scss';

const ACTIVITY_TO_ICON = {
  phone:        'phone',
  meeting:      'event_note',
  video:        'videocam',
  doNotDisturb: 'close',
  car:          'car',
  train:        'train',
  bus:          'bus',
  plane:        'flight',
};

const getActivityIcon = (activity) => ACTIVITY_TO_ICON[activity];

const getActivityText = (activity) => {
  const now = moment();

  if (activity.end) {
    const end = moment(activity.end);
    const duration = moment.duration(end.diff(now)).humanize();

    return `end in ${duration}`;
  }

  if (activity.start) {
    const start = moment(activity.start);
    return moment.duration(start.diff(now)).humanize(true);
  }

  return '';
};

const Activity = ({
  active,
  activity,
  doNotDisturb,
  right = false,
}) => (
  <div className={cs('activity', { 'mod-right': right })}>
    <div className="activity-wrapper">
      { !active && <div className="activity-text mod-big">Off</div> }
      { active && doNotDisturb && (
        <Fragment>
          <i className="material-icons activity-icon">
            { getActivityIcon('doNotDisturb') }
          </i>
        </Fragment>
      ) }
      { active && !doNotDisturb && activity && (
        <Fragment>
          { getActivityIcon(activity.type) && (
            <i className="material-icons activity-icon">
              { getActivityIcon(activity.type) }
            </i>
          ) }
          { (activity.end || activity.start) && (
            <div className="activity-text">
              { getActivityText(activity) }
            </div>
          ) }
        </Fragment>
      ) }
    </div>
  </div>
);

export default Activity;
