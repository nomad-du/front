import React from 'react';

import './Header.scss';

const Header = () => (
  <header className="header">
    <div className="header-title">MAIF</div>
  </header>
);

export default Header;
