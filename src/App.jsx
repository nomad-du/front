/* eslint-disable jsx-a11y/accessible-emoji */
import React, { Fragment } from 'react';
import orderBy from 'lodash/orderBy';
import reverse from 'lodash/reverse';
import values from 'lodash/values';
import isEqual from 'lodash/isEqual';

import { connect } from 'react-redux';
import { geolocated } from 'react-geolocated';

import * as actions from './actions';

import State from './components/State';
import Header from './components/Header';
import Me from './components/Me';
import Welcome from './components/Welcome';
import Notifications from './components/Notifications';

const Modal = ({ onClick }) => (
  <div className="modal">
    <div>
      <p>On a besoin du bluetooth pour le proto 😊</p> <br />
      <button className="modal-button" onClick={onClick}>
        Ok, pas de problème
      </button>
    </div>
  </div>
);

const States = ({
  users,
  me,
  notifications,
  logout,
  doNotDisturbOn,
  doNotDisturbOff,
  updateMood,
}) => (
  <Fragment>
    <Notifications users={values(notifications)} />
    <Header />
    <div className="app-states">
      { users.map((user) => <State key={user.user_id} user={user} />) }
    </div>
    <div className="app-me">
      <Me
        user={me}
        logoutClick={() => logout(me.id)}
        doNotDisturbClick={(active) => (active
          ? doNotDisturbOn(me.id)
          : doNotDisturbOff(me.id)
        )}
        updateMood={(mood) => updateMood(me.id, mood)}
      />
    </div>
  </Fragment>
);

const WelcomeScreen = ({ me, connectUser }) => (
  <div className="app-welcome">
    <Welcome user={me} connectUser={connectUser} />
  </div>
);

class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    const currentPosition = {
      coords:                 this.props.coords,
      isGeolocationAvailable: this.props.isGeolocationAvailable,
      isGeolocationEnabled:   this.props.isGeolocationEnabled,
      positionError:          this.props.positionError,
    };

    const nextPosition = {
      coords:                 nextProps.coords,
      isGeolocationAvailable: nextProps.isGeolocationAvailable,
      isGeolocationEnabled:   nextProps.isGeolocationEnabled,
      positionError:          nextProps.positionError,
    };

    if (!isEqual(currentPosition, nextPosition)) {
      if (nextPosition.positionError) {
        console.error(nextPosition.positionError);
        return;
      }

      if (!nextPosition.isGeolocationAvailable
        || !nextPosition.isGeolocationEnabled) {
        console.error(
          'isGeolocationAvailable', nextPosition.isGeolocationAvailable,
          'isGeolocationEnabled', nextPosition.isGeolocationEnabled
        );
        return;
      }

      if (this.props.me.id) {
        this.props.updateGeolocation(this.props.me.id, nextPosition.coords);
      }
    }
  }

  render() {
    const {
      modal,
      onModalClose,
      loading,
      users,
      me,
      notifications,
    } = this.props;

    return (
      <Fragment>
        <div className="app-wrapper">
          { modal && <Modal onClick={onModalClose} /> }
          { !modal && !loading && me.active && (
            <States
              users={users}
              me={me}
              notifications={notifications}
              logout={this.props.logout}
              doNotDisturbOn={this.props.doNotDisturbOn}
              doNotDisturbOff={this.props.doNotDisturbOff}
              updateMood={this.props.updateMood}
            />
          )}
          { !modal && !loading && !me.active && (
            <WelcomeScreen
              me={me}
              connectUser={() => this.props.login(me.id)}
            />
          )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  users:         reverse(orderBy(state.users, (u) => u.lastUpdate)),
  me:            state.me,
  loading:       !state.me || state.me.name === undefined,
  notifications: state.notifications,
  modal:         state.modal,
});

const geolocatedApp = geolocated({
  positionOptions: {
    enableHighAccuracy: true,
    maximumAge:         2000, // 2 sec cache
    timeout:            Infinity,
  },
  watchPosition:           true,
  suppressLocationOnMount: false,
  userDecisionTimeout:     5000,
})(App);

export default connect(
  mapStateToProps,
  actions
)(geolocatedApp);

