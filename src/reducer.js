import extend from 'lodash/extend';
import omit from 'lodash/omit';

import {
  SET_USER,
  ADD_NEW_USER,
  SAVE_UPDATE_USER,
  NEW_NOTIF,
  REMOVE_NOTIF,
  SHOW_MODAL,
} from './actions';

const initialState = {
  users:         {},
  me:            null,
  notifications: {},
};

const reducer = (state = initialState, action) => {
  if (action.type === SET_USER) {
    return {
      ...state,
      me: {
        id: action.id,
      },
    };
  }

  if (action.type === ADD_NEW_USER) {
    if (action.id === state.me.id) {
      return {
        ...state,
        me: action.user,
      };
    }

    return {
      ...state,
      users: {
        ...state.users,
        [action.id]: action.user,
      },
    };
  }

  if (action.type === SAVE_UPDATE_USER) {
    const user = state.users[action.id];
    const me = state.me.id === action.id && state.me;

    if (me) {
      return {
        ...state,
        me: extend(me, action.user),
      };
    }

    return {
      ...state,
      users: {
        ...state.users,
        [action.id]: extend(user, action.user),
      },
    };
  }

  if (action.type === NEW_NOTIF) {
    return {
      ...state,
      notifications: {
        ...state.notifications,
        [action.id]: action.user,
      },
    };
  }

  if (action.type === REMOVE_NOTIF) {
    return {
      ...state,
      notifications: {
        ...omit(state.notifications, action.id),
      },
    };
  }

  if (action.type === SHOW_MODAL) {
    return {
      ...state,
      modal: action.modal,
    };
  }

  return state;
};

export default reducer;
