/* global SOCKET_URL */

import openSocket from 'socket.io-client';
import {
  addNewUser,
  updateUser,
} from '../actions';

const socketManager = (store) => {
  console.log('Connecting socket to', SOCKET_URL);
  const io = openSocket(SOCKET_URL);

  io.on('connection', (socket) => {
    console.log('socket connection');

    socket.on('disconnect', () => {
      console.log('socket disconnected');
    });
  });

  io.on('users', (users) => {
    console.log('socket users event', users);
    users.forEach((user) => store.dispatch(addNewUser(user.id, user)));
  });

  io.on('update', (id, user) => {
    console.log('socket update event', id, user);
    store.dispatch(updateUser(id, user));
  });
};

export default socketManager;
