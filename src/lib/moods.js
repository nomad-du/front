const MOOD_COLORS = [
  { r: 255, g: 255, b: 255 }, // white
  { r: 45, g: 156, b: 219 }, // blue
  { r: 40, g: 170, b: 90 }, // green
  { r: 235, g: 87, b: 87 }, // red
  { r: 255, g: 190, b: 50 }, // yellow
];

const MOOD_COLORS_ARDUINO = [
  { r: 255, g: 255, b: 255 }, // white
  { r: 2, g: 5, b: 255 }, // blue
  { r: 20, g: 255, b: 1 }, // green
  { r: 255, g: 1, b: 1 }, // red
  { r: 220, g: 200, b: 1 }, // yellow
];

const MOOD_WHITE = 0;

export const getMoodColorPure = (moods) => (mood) => (
  moods[mood] || moods[MOOD_WHITE] // white as default
);

export const getMoodColor = getMoodColorPure(MOOD_COLORS);

export const getMoodColorArduino = getMoodColorPure(MOOD_COLORS_ARDUINO);

export const getMoodColorStr = (mood) => {
  const color = getMoodColor(mood);

  return `rgb(${color.r}, ${color.g}, ${color.b})`;
};
