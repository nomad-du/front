/* global API_URL */
import request from 'request';

const sendUpdateUser = (id, update) => new Promise((resolve, reject) => {
  console.log(`POST /states/${id}/update`, update);
  request.post({
    baseUrl: API_URL,
    uri:     `/states/${id}/update`,
    json:    update,
  }, (err, res, body) => {
    if (err) {
      console.error(err);
      reject(err);
      return;
    }

    resolve(body);
  });
});

export const sendGeolocation = (id, coords) => sendUpdateUser(id, {
  location: {
    lng:      coords.longitude,
    lat:      coords.latitude,
    accuracy: coords.accuracy,
    heading:  null,
    speed:    null,
  },
});

export const sendActive = (id, active) => sendUpdateUser(id, {
  active: active ? { start: new Date() } : false,
});

export const sendDoNotDisturb = (id, doNotDisturb) => sendUpdateUser(id, {
  doNotDisturb: doNotDisturb ? { start: new Date() } : false,
});

export const sendMood = (id, mood) => sendUpdateUser(id, {
  mood,
});

export const getUserState = (id) => new Promise((resolve, reject) => {
  console.log(`GET /states/${id}`);
  request.get({
    baseUrl: API_URL,
    uri:     `/states/${id}`,
    json:    true,
  }, (err, res, body) => {
    if (err) {
      console.error(err);
      reject(err);
      return;
    }

    resolve(body);
  });
});

