/* eslint-disable import/prefer-default-export */
const BT_OBJECT_NAME = 'wuipbthm10';
const BT_SERVICE_UUID = '0000ffe0-0000-1000-8000-00805f9b34fb';
const BT_CHARACTERISTIC_UUID = '0000ffe1-0000-1000-8000-00805f9b34fb';

const btCallbacks = [];
let btCharacteristic = null;

export const createBluetoothListener = () => new Promise((resolve, reject) => {
  const listener = {
    onCharacteristicChange: (callback) => btCallbacks.push(callback),
    sendCharacteristic:     (text) => {
      const encoder = new TextEncoder('utf-8');
      const msg = encoder.encode(`\n${text}\n`);

      return btCharacteristic && btCharacteristic.writeValue(msg);
    },
  };

  navigator.bluetooth
    .requestDevice({
      acceptAllDevices: true,
      optionalServices: [BT_SERVICE_UUID],
    })
    .then((device) => {
      console.log('found device', device.name);
      if (device.name === BT_OBJECT_NAME) {
        return device.gatt.connect();
      }

      return new Error('Not nomad. object');
    })
    .then((server) => server.getPrimaryService(BT_SERVICE_UUID))
    .then((service) => service.getCharacteristic(BT_CHARACTERISTIC_UUID))
    .then((characteristic) => {
      characteristic.startNotifications().then(() => {
        btCharacteristic = characteristic;

        characteristic.addEventListener(
          'characteristicvaluechanged',
          (event) => {
            const { value } = event.target;
            const decoder = new TextDecoder('utf-8');
            const decodedValue = decoder.decode(value);
            console.log('received', decodedValue);

            btCallbacks.forEach((callback) => callback(decodedValue));
          }
        );

        console.log('Notifications have been started.');
        resolve(listener);
      });
    })
    .catch((error) => {
      console.error(error);
      reject(error);
    });
});

